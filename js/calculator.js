
var calculator = function() {
	var startZone = $('#startZone').find(":selected").val();
  		var endZone = $('#endZone').find(":selected").val();
  		if (!startZone) {
        Materialize.toast('Odaberite polaznu lokaciju', 4000, 'greyToast');
  			return null;
  		}

      if (!endZone) {
        Materialize.toast('Odaberite odredišnu lokaciju', 4000, 'greyToast');
        return null;
      }
  		var price = calculate(startZone, endZone);
  		$('#numericPrice').html(price);
  		$('#price-display').removeClass('hidden');
  	};
var prices = {
	'1' : '350',
	'2' : '500',
	'3' : '500',
	'4' : '600',
	'5' : '800',
	'6' : '900',
	'7' : '1100'
};
var calculate = function(startZone, endZone) {
	var zone = endZone;
	if (endZone < startZone) {
		zone = startZone;
	}
	return prices[zone];
};