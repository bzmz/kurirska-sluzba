(function($){
  $(function(){
    var screenHalfWidth = $( window ).width() / 2;
    $('.button-collapse').sideNav({
      menuWidth: screenHalfWidth,
      closeOnClick: true,
      draggable: true
    });
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    $('#year').html(currentYear);
    $('.parallax').parallax();
    $(".dropdown-button").dropdown();
  	$('.slider').slider({full_width: true, interval: 2500});
  	$('select').material_select();
    $('.modal').modal({
      startingTop: '-8%'
    });
  	$('#calculate-btn').click(function() {
      calculator();
  	});
    $('a[href^="#"]').on('click keyup', function(event){ 
	    event.preventDefault();
	    var o =  $( $(this).attr("href") ).offset();   
	    var sT = o.top - $(".navbar-fixed").outerHeight(true);
	    window.scrollTo(0,sT);
	});
  });
})(jQuery);
